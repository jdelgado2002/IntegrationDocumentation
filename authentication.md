##Authentication
> All request require a secure connection over https.  

>**Note** We usually work with the following:
>
> - Basic authentication
> - Token based authentication
> - Authentication as part of the Payload (i.e. send username and password as part of the request payload)