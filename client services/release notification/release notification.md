##release notification

Web service to be notified about a vehicle's boot being released.  This might be followed by a call to the tow notification service, since we release the boot from the vehicle in order to tow the vehicle.

Request| Response
-------|---------
[ReleaseRequestDto](./release request.md)|	[ReleaseResponseDto](./release response.md)