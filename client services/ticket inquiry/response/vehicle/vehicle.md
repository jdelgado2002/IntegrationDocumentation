###vehicle dto

Property |	Required | Type	|Description
-------- |  -------- | -----|-----------
Plate |	Y |	string |	License plate number
State |	Y |	string |	License plate state
PlateType |	N |	string |	License plate type
Make |	N |	string |	vehicle make
Color |	N |	string |	vehicle color
OwnerFirstName |	Y |	string |	vehicle owner first name
OwnerLastName |	Y |	string |	vehicle owner last name
OwnerAddress1 |	N |	string |	vehicle owner address 1
OwnerAddress2 |	N |	string |	vehicle owner address 2
OwnerCity |	N |	string |	vehicle owner city
OwnerState |	N |	string |	vehicle owner state
OwnerZip |	N |	string |	vehicle owner zip
VIN |	N |	string |	vehicle vin

