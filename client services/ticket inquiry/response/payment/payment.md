###payment dto

> You have the ability to send all payment information, including our own payments, on every request.  If including our own payments, we recommend adding extra fields to this payload that represent unique identifiers on 
> your system in addition to the ones that represent unique identifiers on Paylock's system.
> Another approach is to have an indicator such as a boolean value or tag that indicates the source of the payment.

Property |	Required |	Type |	Description
---------|-----------|-------|--------------
Amount |	Y |	decimal |	Total amount paid by customer on this transaction. i.e. a payment for $1000 was taken, out of those $1000, $90 get applied to the ticket containing this payment entry. This field will then have 1000.00
PaymentDate |	Y |	dateTime |	UTC Date the payment was taken
TicketPaymentAmount |	Y |	decimal |	Payment amount applied to the ticket. i.e. a payment for $1000 was taken, out of those $1000, $90 get applied to the ticket containing this payment entry. This field will then have 90.00
TransactionNumber |	Y |	string |	Unique identifier for this payment transaction. If the payment being sent is a paylock payment, the id must be the id sent to you on the payment notification, otherwise, send a unique identifier created by your system. This is the only way to search for payments in order to perform CRUD operations on them.
