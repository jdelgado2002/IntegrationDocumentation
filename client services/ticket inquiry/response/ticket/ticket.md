###ticket dto

Property |	Required |	Type |	Description
---------|-----------|-------|-------------
CustomerAccountNumber |	N |	string |	Used for other unique identifiers besides the TicketNumber field. (if the client keeps different identifiers, this is the place for them)
Description |	N |	string |	Ticket Description
Payments |	N |	List<[Payment](../payment/payment.md)> |	List of any payments currently applied to this specific ticket
TicketAmount |	Y |	decimal |	current ticket amount. This is not the amount due. e.g. ticket = $100, late fee = $20, and $30 partial payment. This field will have $100 + $20 = $120. The partial payment will be shown on the payments collection. The amount due is calculated by Paylock based on the ticket amount minus any payments
TicketNumber |	Y |	string |	Unique identifier for the ticket.
TicketType |	Y |	string |	Type of ticket such as red light, parking, speeding, etc
OwnerLastName |	Y |	string |	vehicle owner last name
OwnerAddress1 |	N |	string |	vehicle owner address 1
OwnerAddress2 |	N |	string |	vehicle owner address 2
OwnerCity |	N |	string |	vehicle owner city
OwnerState |	N |	string |	vehicle owner state
OwnerZip |	N |	string |	vehicle owner zip
VIN |	N |	string |	vehicle vin