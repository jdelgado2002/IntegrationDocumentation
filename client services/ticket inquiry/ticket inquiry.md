###Ticket inquiry service

This service is hosted by our partners.  The bootview application will initiate communication by sending a request containing mostly vehicle information.  
The service will respond with a collection of tickets, payments, and any extra information deemed necessary by the specifics of the contract.


|Request| Response|
|:-------|:-------|
|[TicketInquiryRequestDTO](./request/ticket inquiry request.md)|[TicketInquiryResponseDTO](./response/ticket inquiry response.md) |
