###boot notification request

Property |	Required |	Type |	Description
---------|-----------|-------|--------------
BootCrew |	N |	string |	name for the booting officer placing the boot
BootDate |	Y |	DateTime |	DateTime when the boot was placed on the vehicle
BootFeeAmount |	Y |	decimal |	Paylock's boot fee being charged to the motorist
ClientCode |	Y |	string |	3 letter code for the client municipality
Location |	Y |	string |	Location where the boot action happened
Password |	Y |	string |	Authentication Password
PaylockCaseNumber |	Y |	string |	Unique string identifying the case number created by Paylock in order to keep track of the boot attempt (50 characters)
Plate |	Y |	string |	Booted vehicle license plate
PlateType |	N |	string |	Booted vehicle plate type
State |	Y |	string |	Booted vehicle state
UserId |	Y |	string |	Authentication User Id
