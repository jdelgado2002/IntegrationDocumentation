##Client services

This section contains descriptions for the possible communications started by Paylock to either 
request or post data to our partners.  
----------

> Please note that we are leaving out any notion of what an endpoint might look like (RPC vs REST endpoints) or a 
> representation in a specific format (JSON vs XML) so that we can concentrate in actual information being transfered.