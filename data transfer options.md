##Data transfer options

All our communications will need to happen via https.

>**Note** We usually work with the following:

> - Webhooks
> - RESTful endpoints
> - SOAP RPC calls